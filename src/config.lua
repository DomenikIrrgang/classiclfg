ClassicLFG.Config = {
    Font = "Fonts\\FRIZQT__.ttf",
    Network = {
        Prefix = "CLFG",
        Channel = {
            Name = "ClassicLFGNetwork",
            Id = 1
        },
        Prefixes = {
            SendData = "CLFG_Response",
            RequestData = "CLFG_Request",
            PostGroup = "CLFG_PostGroup",
            DequeueGroup = "CLFG_DQGroup"
        }
    }
}